package net.pl3x.bukkit.kachigga;

import java.io.IOException;
import java.util.logging.Level;

import net.pl3x.bukkit.kachigga.commands.CmdKachigga;
import net.pl3x.bukkit.kachigga.listeners.PlayerListener;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;
import org.mcstats.Metrics;

public class Main extends JavaPlugin {
	public void onEnable() {
		saveDefaultConfig();

		Bukkit.getPluginManager().registerEvents(new PlayerListener(this), this);

		getCommand("kachigga").setExecutor(new CmdKachigga(this));

		try {
			Metrics metrics = new Metrics(this);
			metrics.start();
		} catch (IOException e) {
			log("&4Failed to start Metrics: &e" + e.getMessage());
		}

		log(getName() + " v" + getDescription().getVersion() + " by BillyGalbreath enabled!");
	}

	public void onDisable() {
		log(getName() + " Disabled.");
	}

	public void log(String msg) {
		if (getConfig().getBoolean("color-logs", true)) {
			getServer().getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&3[&d" + getName() + "&3]&r " + msg));
			return;
		}
		Bukkit.getLogger().log(Level.INFO, "[" + getName() + "] " + ChatColor.stripColor(msg));
	}
}
