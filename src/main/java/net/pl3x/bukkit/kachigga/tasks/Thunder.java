package net.pl3x.bukkit.kachigga.tasks;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class Thunder implements Runnable {
	private Player player;

	public Thunder(Player player) {
		this.player = player;
	}

	@Override
	public void run() {
		for (Player target : Bukkit.getOnlinePlayers()) {
			target.playSound(target.getLocation(), Sound.AMBIENCE_THUNDER, 10.0F, 1.0F);
		}
		player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6Kachigga!"));
	}

}
